#!/bin/bash

if [ -f /vagrant/someInfo.txt ]; then
	#echo "File exists, want to overwrite?"
	read -p "File exists, want to overwrite? " word
	yes="y"
	if [ $word = $yes ]; then
		ip addr show > someInfo.txt
		w >> someInfo.txt
		echo "File has been made"
	else
		echo "File not made"
	fi
else
	ip addr show > someInfo.txt
	w >> someInfo.txt
fi

